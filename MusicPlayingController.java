//音乐播放控制器界面布局文件：
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:layout_marginTop="8dip"
    android:background="@drawable/music_bg"
    android:orientation="vertical" >
    
    <LinearLayout android:orientation="horizontal"
        android:layout_width="match_parent"
        android:layout_height="50dip">
        <TextView android:id="@+id/music_info"
        	android:layout_width="match_parent"
        	android:layout_height="wrap_content"
        	android:layout_marginRight="15dip"
        	android:gravity="ri"
        	android:text=""
        	android:textColor="@color/white" />
     </LinearLayout>
     
    <LinearLayout android:orientation="horizontal"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:layout_marginRight="5dip"
        android:gravity="right" >
    	<ImageButton android:id="@+id/previousBtn"
        	android:layout_width="50dip"
        	android:layout_height="50dip"
        	android:layout_toRightOf="@+id/music_info"
        	android:layout_marginLeft="60dip"
        	android:layout_marginTop="15dip"
        	android:scaleType="fitXY"  
        	android:background="@android:color/transparent"
        	android:src="@drawable/upre"/>
    
    	<ImageButton  android:id="@+id/playBtn"
        	android:layout_width="50dip"
        	android:layout_height="50dip"
        	android:layout_toRightOf="@+id/previousBtn"
        	android:layout_marginLeft="20dip"
        	android:layout_marginTop="15dip"
        	android:scaleType="fitXY"  
        	android:background="@android:color/transparent"
        	android:src="@drawable/uplay" />
    
   		<ImageButton android:id="@+id/nextBtn"
        	android:layout_width="50dip"
        	android:layout_height="50dip"
        	android:layout_toRightOf="@+id/playBtn"
        	android:layout_marginLeft="20dip"
        	android:layout_marginTop="15dip"
        	android:scaleType="fitXY"  
        	android:background="@android:color/transparent"
        	android:src="@drawable/unext"/>
    </LinearLayout>
</LinearLayout>

//主界面代码：
public class MainActivity extends Activity {
        private MusicHelper mMusicHelper;
        private ImageButton preBtn;
	private ImageButton playBtn;
	private ImageButton nextBtn;
	private TextView musicInfo;
        @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mMusicHelper = new MusicHelper(MainActivity.this);
		setContentView(R.layout.activity_main);
                musicInfo = (TextView) findViewById(R.id.music_info);
		musicInfo.setText(mMusicHelper.getMusicName());
		
		preBtn = (ImageButton) findViewById(R.id.previousBtn);
		playBtn = (ImageButton) findViewById(R.id.playBtn);
		if (mMusicHelper.isPlaying()) {
			playBtn.setImageDrawable(getResources().getDrawable(R.drawable.stop));
		} else {
			playBtn.setImageDrawable(getResources().getDrawable(R.drawable.play));
		}
		nextBtn = (ImageButton) findViewById(R.id.nextBtn);
		preBtn.setOnClickListener(musicListener);
		playBtn.setOnClickListener(musicListener);
		nextBtn.setOnClickListener(musicListener);
	}

        private OnClickListener musicListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.previousBtn :
				mMusicHelper.prevMusic();
				musicInfo.setText(mMusicHelper.getMusicName());
				Log.d(LOG_TAG, "PREVIOUS BUTTON PRESSED...");
				break;
			case R.id.playBtn :
				if (mMusicHelper.isPlaying()) {
					playBtn.setImageDrawable(getResources().getDrawable(R.drawable.play));
					mMusicHelper.pause();
				} else {
					playBtn.setImageDrawable(getResources().getDrawable(R.drawable.stop));
					mMusicHelper.playMusic();
				}
				Log.d(LOG_TAG, "play button pressed...");
				break;
			case R.id.nextBtn :
				mMusicHelper.nextMusic();
				musicInfo.setText(mMusicHelper.getMusicName());
				Log.d(LOG_TAG, "next button pressed...");
			default :
				break;
			}
			
		}
	};
   
}

//音乐播放控制代码：
public class MusicHelper {
	
	public static final String LOG_TAG = "MusicHelper";
	private Context mContext;
	
	/* 播放器对象 */
	public MediaPlayer mMediaPlayer;
	
	/* 音乐列表 */
	public List<String> mMusicList;
	
	/* 当前播放的歌曲索引 */
	public int currentPlayingItem = 0;
	
	/* 当前播放的位置 */
	public long currentPosition = 0;
	
	/* 是否自动播放下一曲 */
	public boolean isPlayingNext = false;
	
	/* 是否暂停了 */
	public boolean isPauseed = false;
	
	public MusicHelper(Context context) {
		this.mContext = context;
		mMediaPlayer = new MediaPlayer();
		mMusicList = new ArrayList<String>();
		getAllMusicFiles(Constants.MEDIA_PATH);
	}
	
	/**
	 * 获取SD卡中的所有mp3音乐
	 * @param pathname the directory path
	 * @return List the array of all files
	 * */
	public List<String> getAllMusicFiles(String pathname) {
		if (pathname == null || pathname.length() < 1) {
			System.out.println("FileHelper.getAllMusicFiles, inavailable path when retrive a path");
			return null;
		}
		File mFile = new File(pathname);
		if (mFile.exists()) {
			if (mFile.isDirectory()) {
				File[] files = mFile.listFiles();
				if (files != null) {
					for (File file : files) {
						if (file.isDirectory()) {
							getAllMusicFiles(file.getPath()); // 递归搜索
						} else {
							if (file.getAbsolutePath().endsWith(".mp3")) {
								mMusicList.add(file.getAbsolutePath());
							}
						}
					}
				} else {
					System.out.println("there are no any files under the path.");
				}
			} else {
				System.out.println("not a directory." );
			}
		} else {
			System.out.println("file not exists...");
		}
		return mMusicList;
	}
	
	/* 开始播放音乐 */
	public boolean playMusic() {
		Log.d(LOG_TAG, "playMusic()-->currentPlayingItem=" + currentPlayingItem);
		if (mMusicList == null || mMusicList.size() < 1) return false;
		try {
			if (!isPauseed) {
				mMediaPlayer.reset();
				mMediaPlayer.setDataSource(mMusicList.get(currentPlayingItem));
				mMediaPlayer.prepare();
			}
			mMediaPlayer.start();
			
			//自动播放下一首
			mMediaPlayer.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {
					isPlayingNext = true;
					isPauseed = false;
					nextMusic();
					Intent intent = new Intent("com.unistrong.uniteqlauncher.AUTO_PLAY_NEXT_MUSIC");
					mContext.sendBroadcast(intent);//发送广播通知歌名UI更新
				}
			});
		} catch (IllegalStateException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/* 上一曲 */
	public void prevMusic() {
		Log.d(LOG_TAG, "prevMusic()-->currentPlayingItem=" + currentPlayingItem);
		isPauseed = false;
		if (mMusicList == null || mMusicList.size() < 1) return;
		//如果当前歌曲为第一首,则上一曲为最后一首
		if (currentPlayingItem-- <= 0) {
			currentPlayingItem = mMusicList.size() - 1;
		}
		//如果是播放状态,则自动播放上一首
		if (mMediaPlayer.isPlaying()) {
			playMusic();
		} else if (isPlayingNext) {
			playMusic();
		} 
	}
	
	public void nextMusic() {
		Log.d(LOG_TAG, "nextMusic()-->currentPlayingItem=" + currentPlayingItem);
		isPauseed = false;
		if (mMusicList == null || mMusicList.size() < 1) return;
		if (currentPlayingItem++ >= mMusicList.size() - 1) {
			currentPlayingItem = 0;
		}
		if (mMediaPlayer.isPlaying()) {
			playMusic();
		} else if (isPlayingNext) {
			playMusic();
			isPlayingNext = false;
		} 
	}
	
	public boolean pause() {
		isPauseed = true;
		if(mMediaPlayer.isPlaying()){
			mMediaPlayer.pause();
		}else{
			mMediaPlayer.start();
		}
		return false;
	}
	
	public void stop() {
		mMediaPlayer.reset();
	}
	
	public void release() {
		if (mMediaPlayer != null) {
			mMediaPlayer.stop();
			mMediaPlayer.release();
		}
			
	}
	
	public boolean isPlaying() {
		if (mMediaPlayer != null) {
			return mMediaPlayer.isPlaying();
		}
		return false;
	}
	
	/* 获取当前播放的位置,已播放的长度 */
	public long getCurrentPosition() {
		if (mMediaPlayer != null) {
			Log.d(LOG_TAG, "currentPosition=" + mMediaPlayer.getCurrentPosition());
			currentPosition =  mMediaPlayer.getCurrentPosition();
			return currentPosition;
		}
		return -1;
	}
	
	/* 定位到具体位置,用于快进或快退 */
	public void seekTo() {
		long position = getCurrentPosition();
		mMediaPlayer.seekTo((int) position);
	}
	
	public String getMusicName() {
		if (mMusicList == null || mMusicList.size() < 1) return "";
		String name = "";
		if (mMusicList != null) {
			name = mMusicList.get(currentPlayingItem);
			name = name.substring(name.lastIndexOf("/") + 1, name.lastIndexOf("."));
		}
		return name;
	}

}